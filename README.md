# pico-space

This is a simple shooter game template that has the following mechanics:
- projectile shooting
- simple collision detection between different sprites
- player health
- player score

## Screenshots

- [First](https://imgur.com/a/Ogd7ebG)
- [Second](https://imgur.com/a/52Z4ToM)

## How to Install

1. Download the 'space_shooter.p8' file
2. Open your Pico-8 application
3. Using Pico-8's command prompt, type 'folder' and hit enter
4. Drag the 'space_shooter.p8' file into the folder that has been opened
5. Finally, type 'load space_shooter' into Pico-8's command prompt
